***Создание проекта***


Предварительно на пк должен быть установлен GO
при необходимости скачиваем необходимые исходники командами в консоли из директории $GOPATH\src

go get github.com/spf13/cobra/cobra

go get github.com/golang/protobuf/protoc-gen-go

go get github.com/boltdb/bolt/...



1. Создаем проект с помощью кобры
2. Создаем proto файл в пакете model
3. Прописываем зависимости для протобуфа из директории $GOPATH/bin/protoc-3.5.0-win32/bin
 protoc -I=$GOPATH\src[имя проекта] --go_out=$GOPATH\src[имя проекта] $GOPATH\src[имя проекта]\model\record.proto (после выполнения у вас в директории $GOPATH\src[имя проекта]\model сгенерится структура record.pb.go)
Перейдите опять в каталог $GOPATH\src[имя проекта]
4. Переходим в каталог созданного проекта и с помощью команды - cobra add [command] создаем основу для нужных нам методов


***Работа с проектом***
Скачать проект - go get gitlab.com/elena.shebaldenkova/phonebookBoltDB.git

Перейти в консоли в папку  %GOPATH%/src/gitlab.com/elena.shebaldenkova/phonebookBoltDB.git

Выполнить команду  -  go run main.go [command] [arguments] в зависимости от желаемых действий:



- addNewRecord [телефон в формате +380991234567] [Имя] [Город] [Улица] [номер дома] [Номер квартиры]
- deleteRecord [телефон в формате +380991234567] 
- findRecord   [телефон в формате +380991234567] 
- updateAdress [телефон в формате +380991234567] [Город] [Улица] [номер дома] [Номер квартиры]
- updateName   [телефон в формате +380991234567] [Имя] 
- viewRecord