package utils

import (
	"log"
	"regexp"
	"time"

	"github.com/boltdb/bolt"
)

func CheckValidNumber(key string) bool {
	var validNumber = regexp.MustCompile(`^[+][3][8][0][0-9\\s\\(\\)\\+]{9}$`)
	val := validNumber.MatchString(key)
	return val
}

func OpenDB(bool) (*bolt.Tx, error) {
	db, err := bolt.Open("my.db", 0600, &bolt.Options{Timeout: 1 * time.Second})
	if err != nil {
		log.Fatal("open db error", err)
		return nil, err
	}
	tx, err := db.Begin(true)
	if err != nil {
		log.Fatal("begin error", err)
		return nil, err
	}

	return tx, nil
}
