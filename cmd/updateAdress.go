package cmd

import (
	"fmt"
	"log"
	"gitlab.com/elena.shebaldenkova/phonebookBoltDB.git/model"
	"gitlab.com/elena.shebaldenkova/phonebookBoltDB.git/utils"

	"github.com/golang/protobuf/proto"
	"github.com/spf13/cobra"
)

// updateAdressCmd represents the updateAdress command
var updateAdressCmd = &cobra.Command{
	Use: "updateAdress",
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) != 5 {
			fmt.Println("Введите номер телефона записи и адрес в формате город улица дом квартира сразу после комманды")
			return
		}
		tx, _ := utils.OpenDB(true)
		defer tx.Rollback()
		bucket, err := tx.CreateBucketIfNotExists([]byte("BOOK"))
		if err != nil {
			log.Fatal("CreateBucketIfNotExists error ", err)
			return
		}
		if rec := bucket.Get([]byte(args[0])); rec != nil {
			record := &model.Record{}
			if err := proto.Unmarshal(rec, record); err != nil {
				log.Fatalln("Failed to parse record:", err)
			}
			record.Adress.Sity = args[1]
			record.Adress.Street = args[2]
			record.Adress.Building = args[3]
			record.Adress.Appartment = args[4]

			if buf, err := proto.Marshal(record); err != nil {
				log.Fatal(err)
				return
			} else if err := bucket.Put([]byte(args[0]), buf); err != nil {
				log.Fatal(err)

				return
			}

			if err := tx.Commit(); err != nil {
				log.Fatal(err)
				return
			}

			fmt.Println("Запись '" + string(args[0]) + "' успешно обновлена")

		} else if rec == nil {
			fmt.Println("Запись под номером " + string(args[0]) + " отсутствует")
		}

	},
}

func init() {
	rootCmd.AddCommand(updateAdressCmd)

}
