package cmd

import (
	"fmt"
	"log"
	"gitlab.com/elena.shebaldenkova/phonebookBoltDB.git/model"
	"gitlab.com/elena.shebaldenkova/phonebookBoltDB.git/utils"

	"github.com/golang/protobuf/proto"
	"github.com/spf13/cobra"
)

// updateNameCmd represents the updateName command
var updateNameCmd = &cobra.Command{
	Use: "updateName",

	Run: func(cmd *cobra.Command, args []string) {
		if len(args) != 2 {
			fmt.Println("Введите номер телефона и имя  записи, которую хотите изменить сразу после комманды")
			return
		}
		tx, _ := utils.OpenDB(true)
		defer tx.Rollback()
		bucket, err := tx.CreateBucketIfNotExists([]byte("BOOK"))
		if err != nil {
			log.Fatal("CreateBucketIfNotExists error ", err)
			return
		}
		if rec := bucket.Get([]byte(args[0])); rec != nil {
			record := &model.Record{}
			if err := proto.Unmarshal(rec, record); err != nil {
				log.Fatalln("Failed to parse record:", err)
			}
			record.Name = args[1]

			if buf, err := proto.Marshal(record); err != nil {
				log.Fatal(err)
				return
			} else if err := bucket.Put([]byte(args[0]), buf); err != nil {
				log.Fatal(err)

				return
			}

			if err := tx.Commit(); err != nil {
				log.Fatal(err)
				return
			}

			fmt.Println("Запись '" + string(args[0]) + "' успешно обновлена")

		} else if rec == nil {
			fmt.Println("запись под номером " + string(args[0]) + " отсутствует")
		}

	},
}

func init() {
	rootCmd.AddCommand(updateNameCmd)
}
