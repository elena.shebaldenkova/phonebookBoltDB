package cmd

import (
	"fmt"
	"log"
	"gitlab.com/elena.shebaldenkova/phonebookBoltDB.git/utils"

	"github.com/spf13/cobra"
)

// deleteRecordCmd represents the deleteRecord command
var deleteRecordCmd = &cobra.Command{
	Use: "deleteRecord",
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) != 1 {
			fmt.Println("Введите после наименования комманды номер телефона записи которую хотите удалить")
			return
		}
		tx, _ := utils.OpenDB(true)
		defer tx.Rollback()
		bucket, err := tx.CreateBucketIfNotExists([]byte("BOOK"))
		if err != nil {
			log.Fatal("CreateBucketIfNotExists error ", err)
			return
		}

		if rec := bucket.Get([]byte(args[0])); rec == nil {
			fmt.Println("Запись под номером " + string((args[0])) + " не найдена")
			return
		} else if err := bucket.Delete([]byte(args[0])); err != nil {
			log.Fatal(err)
			return
		}
		if err := tx.Commit(); err != nil {
			log.Fatal(err)
			return
		}
		fmt.Println("Запись под номером " + string((args[0])) + " удалена")
	},
}

func init() {
	rootCmd.AddCommand(deleteRecordCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// deleteRecordCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// deleteRecordCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
