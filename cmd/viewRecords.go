package cmd

import (
	"fmt"
	"log"
	"gitlab.com/elena.shebaldenkova/phonebookBoltDB.git/model"
	"gitlab.com/elena.shebaldenkova/phonebookBoltDB.git/utils"

	"github.com/golang/protobuf/proto"
	"github.com/spf13/cobra"
)

// viewRecordsCmd represents the viewRecords command
var viewRecordsCmd = &cobra.Command{
	Use: "viewRecords",
	Run: func(cmd *cobra.Command, args []string) {
		tx, _ := utils.OpenDB(false)
		bucket, err := tx.CreateBucketIfNotExists([]byte("BOOK"))
		if err != nil {
			log.Fatal("CreateBucketIfNotExists error ", err)
			return
		}
		bucket.ForEach(func(k, v []byte) error {
			rec := &model.Record{}
			if err := proto.Unmarshal(v, rec); err != nil {
				log.Fatalln("Failed to parse rec:", err)
			}
			fmt.Printf("key=%s, value=%s\n", k, rec)
			return nil
		})
	},
}

func init() {
	rootCmd.AddCommand(viewRecordsCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// viewRecordsCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// viewRecordsCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
