package cmd

import (
	"fmt"
	"log"
	"gitlab.com/elena.shebaldenkova/phonebookBoltDB.git/model"
	"gitlab.com/elena.shebaldenkova/phonebookBoltDB.git/utils"

	"github.com/golang/protobuf/proto"
	"github.com/spf13/cobra"
)

// findRecordCmd represents the findRecord command
var findRecordCmd = &cobra.Command{
	Use: "findRecord",

	Run: func(cmd *cobra.Command, args []string) {
		if len(args) != 1 {
			fmt.Println("Введите номер телефона записи которую хотите найти сразу после комманды")
			return
		}
		tx, _ := utils.OpenDB(true)
		defer tx.Rollback()
		bucket, err := tx.CreateBucketIfNotExists([]byte("BOOK"))
		if err != nil {
			log.Fatal("CreateBucketIfNotExists error ", err)
			return
		}
		if rec := bucket.Get([]byte(args[0])); rec != nil {
			record := &model.Record{}
			if err := proto.Unmarshal(rec, record); err != nil {
				log.Fatalln("Failed to parse record:", err)
			}
			fmt.Printf("key=%s, value=%s\n", string(args[0]), record)
			return
		} else if rec == nil {
			fmt.Println("Номер " + string(args[0]) + " отсутствует")
		}

	},
}

func init() {
	rootCmd.AddCommand(findRecordCmd)
}
