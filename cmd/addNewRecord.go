package cmd

import (
	"fmt"
	"log"
	"gitlab.com/elena.shebaldenkova/phonebookBoltDB.git/model"
	"gitlab.com/elena.shebaldenkova/phonebookBoltDB.git/utils"

	"github.com/golang/protobuf/proto"
	"github.com/spf13/cobra"
)

// addNewRecordCmd represents the addNewRecord command
var addNewRecordCmd = &cobra.Command{
	Use: "addNewRecord",
	Run: func(cmd *cobra.Command, args []string) {
		if !utils.CheckValidNumber(args[0]) && len(args) != 6 {
			fmt.Println("Введенные данные  не соответствует требуемому формату")
			fmt.Println("Введите данные в таком формате [+380991234567] [Имя] [Город] [Улица] [номер дома] [Номер квартиры]")
			return
		}
		//Открываем соединение с бд
		tx, _ := utils.OpenDB(true)
		defer tx.Rollback()

		//открытие записной книги, или создание, если запись будет первой
		bucket, err := tx.CreateBucketIfNotExists([]byte("BOOK"))
		if err != nil {
			log.Fatal("CreateBucketIfNotExists error ", err)
			return
		}
		//проверка по номеру существования записи
		if bucket.Get([]byte(args[0])) != nil {
			println("Запись с таким номером уже существует")
			return
		}
		//заполнение данными модели
		rec := &model.Record{
			Name: args[1],
			Adress: &model.Record_Adress{
				Sity:       args[2],
				Street:     args[3],
				Building:   args[4],
				Appartment: args[5],
			}}
		//сериализация заполненной модели данных для последующей записи в бд
		if buf, err := proto.Marshal(rec); err != nil {
			log.Fatal(err)
			return

		} else if err := bucket.Put([]byte(args[0]), buf); err != nil {
			log.Fatal(err)
			return
		}
		//коммит изменений в бд
		if err := tx.Commit(); err != nil {
			log.Fatal(err)
			return
		}

		fmt.Print(string(args[0]), rec)

	},
}

func init() {
	rootCmd.AddCommand(addNewRecordCmd)
}
